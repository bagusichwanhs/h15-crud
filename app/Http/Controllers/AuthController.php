<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        $fname = $request['nama1'];
        $lname = $request['nama2'];

        return view('halaman.welcome', compact('fname', 'lname'));
    }
}
